package org.softindustry.com.utilities;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class StringUtilities {

    public static String getFormattedDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E, dd MMM yyyy HH:mm:ss zzz");
        ZonedDateTime dateTime = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("GMT"));
        return formatter.format(dateTime);
    }
}
