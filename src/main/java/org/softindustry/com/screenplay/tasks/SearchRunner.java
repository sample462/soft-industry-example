package org.softindustry.com.screenplay.tasks;

import net.serenitybdd.screenplay.Actor;

public class SearchRunner {

    public static void runSearch(String method, String keyword, Actor actor) {
        Search search = null;
        if (method.equalsIgnoreCase("get")) {
            search = new Search(new SearchItemGET(keyword));
        } else if (method.equalsIgnoreCase("post")) {
            search = new Search(new SearchItemPOST(keyword));
        } else if (method.equalsIgnoreCase("put")) {
            search = new Search(new SearchItemPUT(keyword));
        } else if (method.equalsIgnoreCase("patch")) {
            search = new Search(new SearchItemPATCH(keyword));
        } else if (method.equalsIgnoreCase("delete")) {
            search = new Search(new SearchItemDELETE(keyword));
        }
        assert search != null;
        search.execute(actor);
    }

}
