package org.softindustry.com.constant.products;

import java.util.ArrayList;
import java.util.List;

public class ProductFactory {

    public static List<String> getProducts(String product) {
        List<String> products = new ArrayList<>();
        if (product.equalsIgnoreCase("apple")) {
            products = new AppleProduct().getProducts();
        } else if (product.equalsIgnoreCase("orange")) {
            products = new OrangeProduct().getProducts();
        } else if (product.equalsIgnoreCase("cola")) {
            products = new ColaProduct().getProducts();
        } else if (product.equalsIgnoreCase("pasta")) {
            products = new PastaProduct().getProducts();
        }
        return products;
    }



}
