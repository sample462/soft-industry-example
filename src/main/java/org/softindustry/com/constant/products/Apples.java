package org.softindustry.com.constant.products;
public enum Apples {

    APPLE( "apple" );


    public String getTypeOfApple() {
        return typeOfApple;
    }

    private final String typeOfApple;

    Apples(String typeOfApple) {
        this.typeOfApple = typeOfApple;
    }


}
