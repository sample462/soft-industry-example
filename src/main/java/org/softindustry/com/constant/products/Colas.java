package org.softindustry.com.constant.products;
public enum Colas {

    COLA( "cola" ),
    PEPSI("pepsi");

    public String getTypeOfCola() {
        return typeOfCola;
    }

    private final String typeOfCola;

    Colas(String typeOfCola) {
        this.typeOfCola = typeOfCola;
    }

}
